import java.util.*;
import java.io.*;

class StudentColx
{
  private Student[] stu;
  private int numStudents;
  private int accumScore;
  private double accumWeight;
  private int numExams;

  StudentColx(int size)
  {
     stu = new Student[size];
     for (int i=0; i < size; i++)
       stu[i] = new Student();
     numStudents = 0;
     accumScore = 0;
     accumWeight = 0.0;
     numExams = 0;
  }
  public int getAccumScore()
  { return accumScore; }
  public double getAccumWeight()
  { return accumWeight; }
  public int getNumExams()
  { return numExams; }

  void readAnswers(QuestionColx ques, Scanner fin)
  {
     // Read student answers, compare with answer key, tally results:
     //int numExams = 0;
     //accumScore = 0;
     //double accumWeight = 0;
     while (fin.hasNext())
     {
       String buf = stu[numStudents].retrieve(fin, ques);
       double weightedScore = stu[numStudents].getWeightedScore();
       System.out.println("weighted score="+weightedScore);
       if (buf.length() > 1)
       {
         accumScore += stu[numStudents].getNumCorr();
         accumWeight += weightedScore;
         System.out.println("accumScore="+accumScore + " accumWeight = " + accumWeight);
         numExams++;
       }
       numStudents++;
     }
     fin.close();
  }
  void saveResults(PrintWriter resultsOut, double totalWeight)
  {
     resultsOut.printf("%-20s %6s %6s %5s   %s %n",
       "Student","# correct","Weighted","Pct","Answers");  
     resultsOut.printf("%-20s %6s %7s %4s   %s %n",    
        "-------","---------","--------","-----","---------");  
     for (int i=0; i < numStudents; i++)
     {
       double weightedPct = 100 * stu[i].getWeightedScore() /
                            totalWeight;
       resultsOut.printf("%-20s %6d %8.1f  %7.1f  %s %n",
         stu[i].getName(), stu[i].getNumCorr(), 
         stu[i].getWeightedScore(), weightedPct, stu[i].getAnswers());
     }
  }
  void saveResultsForSpreadsheet(PrintWriter resultsOut, double totalWeight)
  {
     resultsOut.println();
     for (int i=0; i < numStudents; i++)
       resultsOut.print(stu[i].getNumCorr()+" ");
     resultsOut.printf("%n %n");
     for (int i=0; i < numStudents; i++)
       resultsOut.printf("%3.1f ",
            100 * stu[i].getWeightedScore() / totalWeight);
     resultsOut.close();
  }
};
