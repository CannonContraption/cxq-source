# Cxq-Source
This repository houses Prof. David Keil's master multiple-choice grading
software. He gave me permission to release it to the public, and so here it is
with a minor bugfix or two.

The primary purpose of this software is to take a couple of input files, namely
the results of students taking a quiz in a course, and output individual and
aggregate data regarding student performance on that quiz.

The original was one massive Java file, so I've also split it into .java files,
roughly one per class.

# Compiling

    javac *.java

# Running the program
Cxq runs interactively, so it should be somewhat self-explanatory. Example
input files are included in the repo for testing.
