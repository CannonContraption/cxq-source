import java.util.*;
import java.io.*;

class QuestionColx
{
   private Question[] question;
   private int occupancy;
   private double weightedScore;

   QuestionColx()
   {
      final int MAX_QUES = 100;
      question = new Question[MAX_QUES];
      // Construct questions
      for (int i=0; i < MAX_QUES; i++)
      {  
        question[i] = new Question();
        question[i].setNum(i+1);
      }
      occupancy = 0;
      weightedScore = 0.0;
   }
   int getCorrAns(int n) { return question[n].getCorrAns(); }
   int readCorrAns(Scanner fin)
      throws FileNotFoundException
   {
      while (question[occupancy].readCorrAns(fin))
         occupancy++;
      return occupancy;
   }
   int getOccupancy()
   {  return occupancy; }
   double getWeight(int n)
   { return question[n].getWeight(); }
   double getWeightedScore()
   { return weightedScore; }
   public int readCorrAnswers(String quiz_name)
      throws FileNotFoundException
   // Read correct answers and question weights from file
   {
      int i = 0;

      String prefix = new String(quiz_name);
      System.out.println("quiz_name=" + quiz_name);     
      i = prefix.indexOf("-");
      if (i > 0)
         prefix = prefix.substring(0,i);
      System.out.println("prefix=" + prefix);     

      // Search file of keys for all quizzes to find this quiz:
      FileReader reader = new FileReader(prefix + "-keys.txt");
      Scanner fin = new Scanner(reader);
      boolean found = false;
      int numQues = 0;
      while (fin.hasNext() && !found)
      {
         String s = fin.next();
         //System.out.println("s=" + s);     

         if (s.startsWith(quiz_name))
         {
            System.out.println("Found " + s);
            numQues = readCorrAns(fin);
            found = true;
         }   
         else
            s = fin.nextLine();
      }
      if (! found)
        System.out.println("Answer key not found");   
      System.out.println("# questions=" + numQues);
      return numQues;
   }
   double getTotalWeight()
   {
     double totalWeight = 0.0;
     for (int j = 0; j < occupancy; j++)
       totalWeight += question[j].getWeight();
     System.out.print("Total wt =" + totalWeight);
     return totalWeight;
   }
   public void save(int num, PrintWriter fout)
   {  question[num].save(fout); }
   public void incrChosen(int n, char ans)
   {  question[n].incrChosen(ans); }
   public void incrNumCorr(int ansNum)
   { question[ansNum].incrNumCorr(); }
  public void incrWeightedScore(int ansNum) 
   { 
     question[ansNum].incrWeightedScore();
     weightedScore += question[ansNum].getWeight();
   }
}
////////////////////////////////////////////////
