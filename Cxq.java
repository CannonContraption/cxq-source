/* Cxq.java: 
   Update of cxquiz.java to use a file that contains answer keys
   for all quizzes.

   Reads a file containing test-taker answers.
   Compares letters in ('a'..'e', 'f', 't') in these files, generates scores, 
   weighted scores, and summary in "-results" file.

   Reads two files to correct a quiz, student answer file and answer key file.

   Format of student answer file: 3 lines not used, followed by list of students,
   with each student's answers concatenated.

   Format of answer key file: a series of lines, each consisting of a quiz file name,
   followed by correct-answers' letters, spaced out, with question weight optional.

   D. Keil 12/19/15
*/
import java.util.*;
import java.io.*;

public class Cxq 
// Application 
{
    public static final int MAX_STU = 100;

    public static void main(String[] args) 
     throws FileNotFoundException
    {
     // Get name of quiz:
     String quiz_name;
     if (args.length > 0)
       quiz_name = args[0];
     else
     {
       System.out.print("Enter quiz name: ");
       Scanner cin = new Scanner(System.in);
       quiz_name = cin.next();
     }

     // Read answer key:
     QuestionColx ques = new QuestionColx();
     int numQues = ques.readCorrAnswers(quiz_name);
     if (numQues == 0)
     {
       System.out.println("Exiting");
       System.exit(0);
     }
     //System.out.println("main. #ques =" + numQues);
     double totalWeight = ques.getTotalWeight();
 
     // Read student answers:
     String file_name = quiz_name + ".txt";
     System.out.println("Reading "+file_name);
     FileReader reader = new FileReader(file_name);
     Scanner fin = new Scanner(reader); 

     // Read 3 lines no longer used:
     String buf = fin.nextLine() + fin.nextLine() + fin.nextLine();

     // Construct collection of student records:
     StudentColx stu = new StudentColx(MAX_STU);

     // Read students' answers, compare with correct answers, tally results:
     stu.readAnswers(ques, fin);

     // Open output file:
     PrintWriter resultsOut = new PrintWriter(quiz_name+"-results.txt");
     resultsOut.println("Quiz: "+quiz_name); 
     resultsOut.printf("%d questions. Total weight: %3.1f %n", 
        numQues, totalWeight);
     resultsOut.println(); 

     // Save average and weighted average:
     double wt_avg = stu.getAccumWeight() / (double)stu.getNumExams();
     double avg = stu.getAccumScore() / (double)stu.getNumExams(),
       avg_pct = 100 * wt_avg / totalWeight;
     resultsOut.printf("%d students. Average: %3.1f  %n", 
       stu.getNumExams(), avg);
     resultsOut.println();      

     System.out.printf("%d students. Average: %3.1f Avg %%= %2.1f %n", 
       stu.getNumExams(), avg, avg_pct);
     resultsOut.printf("Weighted average %%: %3.1f %n", 
                 100 * wt_avg/totalWeight);     

     // Save student results:
     stu.saveResults(resultsOut, totalWeight);

     // Report results by question:
     resultsOut.println("");
     resultsOut.println("Question:");
     for (int j=0; j < numQues; j++)
       ques.save(j,resultsOut);

     // Report student result numbers (for spreadsheet):
     stu.saveResultsForSpreadsheet(resultsOut, totalWeight);
  }
}
