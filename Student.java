import java.util.*;
import java.io.*;

class Student
{
  private String name;
  private String answers;
  private int numCorr; // # correct answers
  private double weightedScore;

  Student() 
  {
    name = new String("");
    answers = new String("");
    numCorr = 0;
    weightedScore = 0.0f;
  }
  public void setName(String nm)  { name = new String(nm); }
  public void incrNumCorr() { numCorr++; }
  public String getName() { return name; }
  public int getNumCorr() { return numCorr; }
  public double getWeightedScore() { return weightedScore; }
  public String getAnswers() { return answers; }
  public void incrWeightedScore(double d) 
  {  weightedScore += d; }

  public String retrieve(Scanner fin, QuestionColx ques)
  // Read student answers, compare with answer key:
  {
    String nm = fin.next(), // Name
           buf = new String("");
    setName(nm);
    //System.out.println("name="+name);
    if (fin.hasNext())
      buf = fin.nextLine(); // Answers
    //System.out.println("line="+buf);
    buf = buf.toUpperCase();    
    buf = buf.replace('T','A');
    buf = buf.replace('F','B');
    for (int i=0; i < buf.length(); i++)
    {
      char c = buf.charAt(i);
      if (c != ' ' && c != '\t')
        answers = answers + c;
    }
    //System.out.println("answers="+answers);
    int ansNum = 0;
    char ans = '\0';
    int j=0; // question number
    if (answers.length() > 0 && answers.charAt(0) != '.')
    {
      while(j < answers.length() && ansNum < ques.getOccupancy())
      {
        ans = answers.charAt(j);
        if ((ans >= 'A' && ans <= 'F') || (ans == 'T'))
        {  
          ques.incrChosen(ansNum, ans);
          if (ans == ques.getCorrAns(ansNum))
          {  
            incrNumCorr();
            incrWeightedScore(ques.getWeight(ansNum));
            ques.incrNumCorr(ansNum);
            ques.incrWeightedScore(ansNum);
          }
          //else
             //System.out.print(" wrong: "+ans+ " s/b "+
             //      ques[ansNum].getCorrAns()+ " ");
        }
        else 
          System.out.println("Invalid answer at " + ansNum);
        ansNum++;
        j++;
      }
      if (ansNum != ques.getOccupancy())
        System.out.println("wrong # Question: " + ansNum);
    }
    return answers;
  }
}
