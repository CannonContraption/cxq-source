import java.util.*;
import java.io.*;

class Question
{
  public static final int NUM_ANS = 5;
  private int num;
  private char corrAns;// in 'a' .. 'e'
  private int numCorr; // number of correct answers
  private double weight;
  private double weightedScore;
  private int[] chosen; // # Students who chose an answer

  Question() 
  {
    num = 0;
    corrAns = '\0';
    numCorr = 0;
    weight = (double)0.0;
    weightedScore = 0.0f;
    chosen = new int[NUM_ANS];
    for (int i=0; i < NUM_ANS; i++)
      chosen[i] = 0;
  }
  public void setNum(int n) { num = n; }
  public int getNum() { return num; }
  public char getCorrAns() { return corrAns; }
  public void setCorrAns(char x) { corrAns = x; }
  public double getWeight() { return weight; }
  public void setWeight(double wt) { weight = wt; }
  public void incrNumCorr() { numCorr++; }
  public void incrWeightedScore() 
      { weightedScore += weight; }
  public void incrChosen(char c) { chosen[c-'A']++;  }
  public int getNumCorr() { return numCorr; }
  //public double getWeightedScore() { return weightedScore; }
  public int getChosen(int n) { return chosen[num]; }
  public boolean readCorrAns(Scanner fin)
  {
    String buf = fin.next();
    //System.out.println("readAns. buf="+buf);
    if (buf.length() > 1 || ! fin.hasNext())
       return false;
    else
    {
      buf = buf.toUpperCase();
      corrAns = buf.charAt(0);
      weight = 1.0;
      if (fin.hasNextDouble()) 
        weight = fin.nextDouble();
    }
    return true;
  }
  public void save(PrintWriter fout)
  {
    fout.printf("%2d. %c wt= %2.1f  %2d correct (", 
      num, corrAns, weight, numCorr);
    for (int i=0; i < Question.NUM_ANS; i++)
      fout.print(chosen[i] + " ");
    fout.println(")");
  }
}
